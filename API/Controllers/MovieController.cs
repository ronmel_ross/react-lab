﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TodoApi.Models;
using System;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace TodoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovieController : ControllerBase
    {
        private readonly TodoContext _context;

        public MovieController(TodoContext context)
        {
            _context = context;

            if (_context.Movies.Count() == 0)
            {
                // Create a new TodoItem if collection is empty,
                // which means you can't delete all TodoItems.
                //_context.TodoItems.Add(new TodoItem { Name = "Item1" });
                //_context.SaveChanges();
                _context.Movies.Add(new Movie
                {
                    Title = "When Harry Met Sally",
                    ReleaseDate = DateTime.Parse("1989-2-12"),
                    Genre = "Romantic Comedy",
                    Rating = "R",
                    Price = 7.99M
                });
                _context.Movies.Add(new Movie
                {
                    Title = "Ghostbusters ",
                    ReleaseDate = DateTime.Parse("1984-3-13"),
                    Genre = "Comedy",
                    Price = 8.99M
                });
                _context.Movies.Add(new Movie
                {
                    Title = "Ghostbusters 2",
                    ReleaseDate = DateTime.Parse("1986-2-23"),
                    Genre = "Comedy",
                    Price = 9.99M
                });
                _context.Movies.Add(new Movie
                {
                    Title = "Rio Bravo",
                    ReleaseDate = DateTime.Parse("1959-4-15"),
                    Genre = "Western",
                    Price = 3.99M
                
                });
                _context.Movies.Add(new Movie
                {
                    Title = "Iron Man",
                    ReleaseDate = DateTime.Parse("2010-4-15"),
                    Genre = "Scifi",
                    Price = 3.99M

                });

                _context.Movies.Add(new Movie
                {
                    Title = "First Blood",
                    ReleaseDate = DateTime.Parse("1983-3-18"),
                    Genre = "Action",
                    Price = 3.99M

                });

                _context.Movies.Add(new Movie
                {
                    Title = "First Blood 2",
                    ReleaseDate = DateTime.Parse("1985-5-22"),
                    Genre = "Action",
                    Price = 3.99M

                });

                _context.Movies.Add(new Movie
                {
                    Title = "Now You See Me",
                    ReleaseDate = DateTime.Parse("2013-5-21"),
                    Genre = "Action",
                    Price = 3.99M

                });

                _context.Movies.Add(new Movie
                {
                    Title = "Now You See Me",
                    ReleaseDate = DateTime.Parse("2013-5-21"),
                    Genre = "Drama",
                    Price = 3.99M

                });

                _context.Movies.Add(new Movie
                {
                    Title = "Drop Zone",
                    ReleaseDate = DateTime.Parse("1994-12-09"),
                    Genre = "Action",
                    Price = 3.99M

                });

                _context.Movies.Add(new Movie
                {
                    Title = "Cutaway",
                    ReleaseDate = DateTime.Parse("2000-12-03"),
                    Genre = "Action",
                    Price = 12.99M

                });

                _context.Movies.Add(new Movie
                {
                    Title = "Terminal Velocity",
                    ReleaseDate = DateTime.Parse("1994-09-23"),
                    Genre = "Suspense",
                    Price = 31.99M

                });

                _context.Movies.Add(new Movie
                {
                    Title = "Chucky",
                    ReleaseDate = DateTime.Parse("1991-09-23"),
                    Genre = "Suspense",
                    Price = 31.99M

                });

                _context.Movies.Add(new Movie
                {
                    Title = "Chucky",
                    ReleaseDate = DateTime.Parse("1991-09-23"),
                    Genre = "Suspense",
                    Price = 31.99M

                });

                _context.Movies.Add(new Movie
                {
                    Title = "Toy story",
                    ReleaseDate = DateTime.Parse("1996-05-01"),
                    Genre = "Cartoons",
                    Price = 11.99M

                });

                _context.SaveChanges();
            }
        }

        
        // GET: api/Movie
        [EnableCors("_myAllowSpecificOrigins")]
        [HttpGet]
        public async Task<IActionResult> Index(string movieGenre, string searchString)
        {
            // Use LINQ to get list of genres.
            IQueryable<string> genreQuery = from m in _context.Movies
                                            orderby m.Genre
                                            select m.Genre;

            var movies = from m in _context.Movies
                         select m;

            if (!string.IsNullOrEmpty(searchString))
            {
                movies = movies.Where(s => s.Title.Contains(searchString));
            }

            if (!string.IsNullOrEmpty(movieGenre))
            {
                movies = movies.Where(x => x.Genre == movieGenre);
            }

            var movieGenreVM = new MovieGenreViewModel
            {
                Genres = new SelectList(await genreQuery.Distinct().ToListAsync()),
                Movies = await movies.ToListAsync()
                //Movies = await movies.Skip(page * pageSize).Take(pageSize).ToListAsync();
                
            };

            //return movieGenreVM;
            //return new IActionResult { "value1", "value2" };
            //return Ok(movieGenreVM.Movies.ToList());
            return Ok(movieGenreVM.Movies.ToList());
        }

        // GET: api/Movie/Genre
        [EnableCors("_myAllowSpecificOrigins")]
        //[HttpGet]
        [HttpGet("Genre")]
        public async Task<IActionResult> GetGenre()
        {
            // Use LINQ to get list of genres.
            IQueryable<string> genreQuery = from m in _context.Movies
                                            orderby m.Genre
                                            select m.Genre;
            var movieGenreVM = new MovieGenreViewModel
            { 
               Genres = new SelectList(await genreQuery.Distinct().ToListAsync()),
            };
            
            //return movieGenreVM;
            //return new IActionResult { "value1", "value2" };
            return Ok(movieGenreVM.Genres.Items);
        } 



        // GET: api/Movie/5
        [EnableCors("_myAllowSpecificOrigins")]
        [HttpGet("{id}")]
        public async Task<ActionResult<Movie>> GetMovie(int id)
        {
           
            var MoviesItem = await _context.Movies.FindAsync(id);

            if (MoviesItem == null)
            {
                return NotFound();
            }

            return MoviesItem;
        }

        // POST: api/Movie
        [EnableCors("_myAllowSpecificOrigins")]
        [HttpPost]
        public async Task<ActionResult<Movie>> PostMovie(Movie item)
        {
            _context.Movies.Add(item);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetMovie), new { id = item.Id }, item);
        }

        // PUT: api/Movie/5
        [EnableCors("_myAllowSpecificOrigins")]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTodoItem(int id, Movie item)
        {
            if (id != item.Id)
            {
                return BadRequest();
            }

            _context.Entry(item).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }

        // DELETE: api/Movie/5
        [EnableCors("_myAllowSpecificOrigins")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            var MovieItem = await _context.Movies.FindAsync(id);

            if (MovieItem == null)
            {
                return NotFound();
            }

            _context.Movies.Remove(MovieItem);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}