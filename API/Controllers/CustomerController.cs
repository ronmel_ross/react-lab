﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TodoApi.Models;
using System;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TodoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly TodoContext _context;

        public CustomerController(TodoContext context) 
        {
            _context = context;

            if (_context.Customers.Count() == 0)
            {
                // Create a new Customer if collection is empty,
                // which means you can't delete all TodoItems.
                
                _context.Customers.Add(new Customer
                {
                    CustLName = "Aguinaldo",
                    CustFName = "Emilio",
                    CustMName =  "A",
                    CustGender = "M",
                    CustBirthDate = DateTime.Parse("1869-2-12"),
                    CustPhoneNo = "P1111111111",
                    CustMobileNo = "M1111111111",
                    CustAddress = "Cavite Philippines",
                    CustActive = true
                });
                _context.Customers.Add(new Customer
                {
                    CustLName = "Quezon",
                    CustFName = "Manuel ",
                    CustMName = "L",
                    CustGender = "M",
                    CustBirthDate = DateTime.Parse("1878-8-19"),
                    CustPhoneNo = "P2222222222",
                    CustMobileNo = "M2222222222",
                    CustAddress = "address 2222222222222",
                    CustActive = true
                });
                _context.Customers.Add(new Customer
                {
                    CustLName = "Osmeña",
                    CustFName = "Sergio  ",
                    CustMName = "L",
                    CustGender = "M",
                    CustBirthDate = DateTime.Parse("1878-9-09"),
                    CustPhoneNo = "P3333333333",
                    CustMobileNo = "M3333333333",
                    CustAddress = "address 3333333333",
                    CustActive = true
                });
                _context.Customers.Add(new Customer
                {
                    CustLName = "Róxas",
                    CustFName = "Manuel   ",
                    CustMName = "A",
                    CustGender = "M",
                    CustBirthDate = DateTime.Parse("1892-1-1"),
                    CustPhoneNo = "P44444444444",
                    CustMobileNo = "M4444444444",
                    CustAddress = "address 4444444444",
                    CustActive = true
                });
                _context.Customers.Add(new Customer
                {
                    CustLName = "Laurel",
                    CustFName = "Jose",
                    CustMName = "P",
                    CustGender = "M",
                    CustBirthDate = DateTime.Parse("1891-3-9"),
                    CustPhoneNo = "P5555555555",
                    CustMobileNo = "M5555555555",
                    CustAddress = "address 5555555555",
                    CustActive = true
                });
                _context.Customers.Add(new Customer
                {
                    CustLName = "Quirino",
                    CustFName = "Elpidio",
                    CustMName = "R",
                    CustGender = "M",
                    CustBirthDate = DateTime.Parse("1890-11-16"),
                    CustPhoneNo = "P66666666666",
                    CustMobileNo = "M6666666666",
                    CustAddress = "address 6666666666",
                    CustActive = true
                });
                _context.Customers.Add(new Customer
                {
                    CustLName = "Magsaysay",
                    CustFName = "Ramon",
                    CustMName = "F",
                    CustGender = "M",
                    CustBirthDate = DateTime.Parse("1907-03-31"),
                    CustPhoneNo = "P7777777777",
                    CustMobileNo = "M7777777777",
                    CustAddress = "address 7777777777",
                    CustActive = false
                });
                _context.Customers.Add(new Customer
                {
                    CustLName = "Garcia",
                    CustFName = "Carlos",
                    CustMName = "P",
                    CustGender = "M",
                    CustBirthDate = DateTime.Parse("1896-11-04"),
                    CustPhoneNo = "P8888888888",
                    CustMobileNo = "M8888888888",
                    CustAddress = "address 8888888888",
                    CustActive = false
                });
                _context.Customers.Add(new Customer
                {
                    CustLName = "Macapagal",
                    CustFName = "Diosdado",
                    CustMName = "P",
                    CustGender = "M",
                    CustBirthDate = DateTime.Parse("1910-09-28"),
                    CustPhoneNo = "P9999999999",
                    CustMobileNo = "M9999999999",
                    CustAddress = "address 9999999999",
                    CustActive = false
                });
                _context.Customers.Add(new Customer
                {
                    CustLName = "Marcos",
                    CustFName = "Ferdinand",
                    CustMName = "E",
                    CustGender = "M",
                    CustBirthDate = DateTime.Parse("1917-09-11"),
                    CustPhoneNo = "P1010101010",
                    CustMobileNo = "M1010101010",
                    CustAddress = "address 1010101010",
                    CustActive = true
                });
                _context.Customers.Add(new Customer
                {
                    CustLName = "Aquino",
                    CustFName = "Corazón",
                    CustMName = "C",
                    CustGender = "F",
                    CustBirthDate = DateTime.Parse("1933-01-02"),
                    CustPhoneNo = "P1212121212",
                    CustMobileNo = "M1212121212",
                    CustAddress = "address 1212121212",
                    CustActive = false
                });
                _context.Customers.Add(new Customer
                {
                    CustLName = "Ramos",
                    CustFName = "Fidel",
                    CustMName = "V",
                    CustGender = "M",
                    CustBirthDate = DateTime.Parse("1928-03-18"),
                    CustPhoneNo = "P1313131313",
                    CustMobileNo = "M1313131313",
                    CustAddress = "address 1313131313",
                    CustActive = true
                });
                _context.Customers.Add(new Customer
                {
                    CustLName = "Estrada",
                    CustFName = "Joseph",
                    CustMName = "M",
                    CustGender = "M",
                    CustBirthDate = DateTime.Parse("1937-04-19"),
                    CustPhoneNo = "P141414141414",
                    CustMobileNo = "M141414141414",
                    CustAddress = "address 1414141414",
                    CustActive = true
                });
                _context.Customers.Add(new Customer
                {
                    CustLName = "Macapagal",
                    CustFName = "Gloria",
                    CustMName = "A",
                    CustGender = "F",
                    CustBirthDate = DateTime.Parse("1947-04-05"),
                    CustPhoneNo = "P1515151515",
                    CustMobileNo = "M1515151515",
                    CustAddress = "address 1515151515",
                    CustActive = true
                });
                _context.Customers.Add(new Customer
                {
                    CustLName = "Aquino",
                    CustFName = "Benigno",
                    CustMName = "C",
                    CustGender = "F",
                    CustBirthDate = DateTime.Parse("1960-02-08"),
                    CustPhoneNo = "P1616161616",
                    CustMobileNo = "M1616161616",
                    CustAddress = "address 1616161616",
                    CustActive = true
                });
                _context.Customers.Add(new Customer
                {
                    CustLName = "Duterte",
                    CustFName = "Rodrigo",
                    CustMName = "R",
                    CustGender = "M",
                    CustBirthDate = DateTime.Parse("1945-03-28"),
                    CustPhoneNo = "P1717171717",
                    CustMobileNo = "M1717171717",
                    CustAddress = "address 1717171717",
                    CustActive = true
                });

                _context.SaveChanges();
            }
        }


        // GET: api/Customer
        [EnableCors("_myAllowSpecificOrigins")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Customer>>> GetCustomers()
        {
            return await _context.Customers.ToListAsync();
        }

        // GET: api/Movie/5
        [EnableCors("_myAllowSpecificOrigins")]
        [HttpGet("{id}")]
        public async Task<ActionResult<Customer>> GetCustomer(int id)
        {


            var CustomerItem = await _context.Customers.FindAsync(id);

            if (CustomerItem == null)
            {
                return NotFound();
            }

            return CustomerItem;
        }

        // POST: api/Customer
        [EnableCors("_myAllowSpecificOrigins")]
        [HttpPost]
        public async Task<ActionResult<Customer>> PostCustomer(Customer item)
        {
            _context.Customers.Add(item);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetCustomer), new { id = item.CustId }, item);
        }

        // PUT: api/Customer/5
        [EnableCors("_myAllowSpecificOrigins")]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCustomer(int id, Customer item)
        {
            if (id != item.CustId)
            {
                return BadRequest();
            }

            _context.Entry(item).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }

        // DELETE: api/Todo/5
        [EnableCors("_myAllowSpecificOrigins")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCustomer(int id)
        {
            var CustomerItem = await _context.Customers.FindAsync(id);

            if (CustomerItem == null)
            {
                return NotFound();
            }

            _context.Customers.Remove(CustomerItem);
            await _context.SaveChangesAsync();

            return NoContent();
        }




    }
}
