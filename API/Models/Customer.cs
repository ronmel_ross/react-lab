﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace TodoApi.Models
{
    public class Customer
    {
        [Key]
        public int CustId { get; set; }

        [Display(Name = "Last Name")]
        [StringLength(60, MinimumLength = 3)]
        [Required]
        public string CustLName { get; set; }

        [Display(Name = "First Name")]
        [StringLength(60, MinimumLength = 3)]
        [Required]
        public string CustFName { get; set; }

        [Display(Name = "Middle Name")]
        [StringLength(60, MinimumLength = 3)]
        [Required]
        public string CustMName { get; set; }

        [Display(Name = "Gender")]
        [StringLength(1)]
        public string CustGender { get; set; }

        [Display(Name = "Birth Date")]
        [DataType(DataType.Date)]
        public DateTime CustBirthDate { get; set; }

        [Display(Name = "Phone No.")]
        [StringLength(15)]
        public string CustPhoneNo { get; set; }

        [Display(Name = "Mobile No.")]
        [StringLength(15)]
        public string CustMobileNo { get; set; }

        [Display(Name = "Address")]
        [StringLength(100)]
        public string CustAddress { get; set; }

        [Display(Name = "Active")]
        public bool CustActive { get; set; }
    }
}
