﻿using Microsoft.EntityFrameworkCore;

namespace TodoApi.Models
{
    public class TodoContext : DbContext
    {
        public TodoContext(DbContextOptions<TodoContext> options)
            : base(options)
        {
        }

        public DbSet<TodoItem> TodoItems { get; set; }
        public DbSet<Movie> Movies { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<Module> Modules { get; set; }

        public DbSet<UserAccess> UsersAccess { get; set; }

        public DbSet<UserSession> UsersSession { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
       {
           base.OnModelCreating(builder);
           builder.Entity<User>().HasKey(p => p.Id);
           builder.Entity<User>().Property(p => p.Username).HasColumnType("varchar(15)").IsRequired();
           builder.Entity<User>().HasIndex(p => p.Username).IsUnique();
           builder.Entity<User>().Property(p => p.Password).HasColumnType("varchar(15)").IsRequired();

           builder.Entity<Module>().HasKey(p => p.Id);
           builder.Entity<Module>().Property(p => p.ModuleName).HasColumnType("varchar(50)").IsRequired();
           builder.Entity<Module>().Property(p => p.ModuleComponent).HasColumnType("varchar(50)").IsRequired();
           builder.Entity<Module>().Property(p => p.ModuleSortId).HasColumnType("varchar(50)").IsRequired();
           builder.Entity<Module>().HasIndex(p => p.ModuleSortId).IsUnique();

           builder.Entity<UserAccess>().HasKey(p => p.Id);
           builder.Entity<UserAccess>().Property(p => p.UserId).IsRequired();
           builder.Entity<UserAccess>().Property(p => p.ModuleId).IsRequired();
           builder.Entity<UserAccess>().Property(p => p.AllowView).HasColumnType("char(1)");
           builder.Entity<UserAccess>().Property(p => p.AllowInsert).HasColumnType("char(1)");
           builder.Entity<UserAccess>().Property(p => p.AllowUpdate).HasColumnType("char(1)");
           builder.Entity<UserAccess>().Property(p => p.AllowDelete).HasColumnType("char(1)");
        }

    }
}